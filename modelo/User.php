<?php
    require_once 'Conexion.php';

    class User{
        private $username;
        private $password;
        private $id_user ;
        private $nomape;

        public function __construct(){

        }

        public function set_Username($username){
            $this->username = $username ;
        }

        public function get_Username(){
            return $this->username ;
        }

        public function get_Id(){
            return $this->id_user ;
        }

        public function get_Nomape(){
            return $this->nomape ;
        }

        public function set_Pass($password){
            $this->password = $password;
        }

        /**
         * Public function Login es una funcion que tomara 2 atributos (user y password) y estos seran verificados
         * con la base de datos para iniciar session de un usuarios, ademas creara una variable de session
         * 
         * RETURN boolean(1,0)
         * 
         */
        public function logIn(){
            
            $pdo = new Conexion();          
            $query = $pdo->prepare("SELECT id_user, nomape FROM usuarios WHERE username = :usuario AND password = :clave");
            $query->bindParam(":usuario" , $this->username);
            $query->bindParam(":clave"   , $this->password);

            $query->execute();

            $num_rows = $query->rowCount();

            if($num_rows > 0){
                $result = $query->fetch(PDO::FETCH_ASSOC);
                $this->id_user = $result['id_user'];
                $this->nomape = $result['nomape'];

                return 1;
            }
            else{
                return 0;
            }

        }
    }

?>