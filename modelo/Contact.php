<?php

    class Contact{

        protected $title ; 
        protected $header;
        protected $from = "Sistema Automatizado de Contacto <no-reply@keysolucionesph.com>";
        protected $replyTo = "no-reply@keysolucionesph.com";
        protected $deliveryTo = 'keysserviciosysoluciones@gmail.com';
        protected $content; 
        public $name ;
        public $telephone ;
        public $email ;
        public $message ;

        public function __construct($arreglo){
            $this->name = $arreglo['name'];
            $this->telephone = $arreglo['telephone'];
            $this->email = $arreglo['email'];
            $this->message = $arreglo['message'];
        }

        public function getName(){
            return $this->name ;
        }

        public function setTitle($titulo){
            $this->title = $titulo;
        } 

        public function getTitle(){
            return $this->header;
        }

        /**
        Funcion para cambiar el remitente del correo 
        Agregar con formato : 'Sistema Automatizado de Contacto <no-reply@keysolucionesph.com>'
        */
        public function setFrom($nuevoProveniente){
            $this->from = $nuevoProveniente;
        } 

        public function getFrom(){
            return $this->from;
        }

        public function setReplyTo($nuevoResponder){
            $this->replyTo = $nuevoResponder;
        } 

        public function getReplyTo(){
            return $this->replyTo;
        }

        public function setHeader(){
            $this->header = "MIME-Version: 1.0\r\n";
            $this->header .= "Content-Type: text/html; charset=UTF-8\r\n";
            $this->header .= "From: ". $this->from ."\r\n";
            $this->header .= "Reply-To: ". $this->replyTo ."\r\n";
        }

        public function getHeader(){
            return $this->header;
        }

        public function setContent(){
            $this->content = "
                <html>
                  <head>
                    <title>". $this->title ."</title>
                  </head>
                  <body>
                    <h1>Contacto de un Visitante!</h1>
                    <h5>De :". $this->name ."</h5>
                    <h5>Responder a: ". $this->email ."</h5>
                    <h5> Llamar a: ". $this->telephone ."</h5>
                    <hr />
                    <p>
                      ". $this->message ."
                    </p>
                    <hr />
                  </body>
                </html>
            ";
        }

        public function Send(){
            $result = mail($this->deliveryTo , $this->title , $this->content , $this->header);
            return $result; 
        }

    }
?>