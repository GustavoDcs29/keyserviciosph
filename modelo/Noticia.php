<?php

  require 'Conexion.php';

  class Noticia{

    private $newsId;
    private $newsTittle;
    private $newsSubtittle;
    private $autor;
    private $content;
    public $fileName;
    private $fileFolder = '../assets/files/';

    public function __construct(){

      $params = func_get_args();

      $num_params = func_num_args();

      $function_constructor =  '__construct' . $num_params ;

      if(method_exists($this , $function_constructor)){
        call_user_func_array(array($this, $function_constructor), $params);
      }

    }

    public function __construct0(){

    }
    /** constructor para los borradores */
    public function __construct1($idNoticiaTemP){

      $this->newsId = $idNoticiaTemP;

    }

    public function __construct2($id , $titulo){

      $this->newsId = $id;

      $this->newsTittle = $titulo;

    }

    public function loadBorrador(){

      $pdo = new Conexion();

      $query = $pdo->prepare("SELECT * FROM temp_noticia WHERE id_temp = :idBorrador");

      $query->bindParam(":idBorrador" , $this->newsId );

      $query->execute();
      
      $datos = $query->fetch(PDO::FETCH_ASSOC);

      return $datos ; 

    }
    /*
      Funcion que creara el post en base de datos utilizando los datos ingresados por el ususario
      que serian el titulo, subtitulo y el autor. La fecha por otra parte sera calculada por el servidor con el objeto
      Date. Una vez recibidos estos datos seran guardados en base de datos con un status de borrador. La funcion retornara
      el ID unico del post para ser utilizado posteriormente en el proceso continuo.
    */
    public function createBorrador($titulo , $subtitulo , $autor , $date){

      $step = 1 ;

      $pdo = new Conexion();
      
      try{

        $query = $pdo->prepare("INSERT INTO temp_noticia (titulo, subtitulo, autor , fecha_creacion , estado)
                                VALUES (:titulo , :subtitulo, :autor, :fecha , :step)");

        $query->bindParam(':titulo' , $titulo);

        $query->bindParam(':subtitulo' , $subtitulo);

        $query->bindParam(':autor' , $autor);

        $query->bindParam(':fecha' , $date);

        $query->bindParam(':step' , $step);

        $query->execute();

        $id = $pdo->lastInsertId();

        return $id ;

      }
      catch(PDOException $e){

        return  $e ;

      }
    }
    /*
      Funcion utilizada para establecer el nombre de los archivos correspondientes
      en donde pone el titulo en mayusculas y le elimina los espacios en blancos.
      al final es concatenado con el ID del elemento noticia para asegurar que sea unico
      el nombre del archivo
    */
    public function setFileName($ext){

      $uppercase = strtoupper($this->newsTittle);

      $titulo = str_replace(' ', '', $uppercase);

      $this->fileName = $this->newsId . $titulo . "." . $ext;

      return $this->fileName;

    }
    /*
      Funcion que se utilizara para guardar en base de datos el nombre del archivo .json
      en donde se encontrara guardada toda la informacion del objeto tipo Noticia
      utilzara el objeto de Conexion
    */
    private function saveFile(){
      $step = 2 ;

      $pdo = new Conexion();

      try {

        $query = $pdo->prepare("UPDATE temp_noticia SET archivo = :contenido , estado = :step WHERE id_temp = " . $this->newsId );

        $query->bindParam(":contenido" , $this->fileName );

        $query->bindParam(":step" , $step);

        $query->execute();

        return True;

      } catch (PDOException $e) {

        return False;

      }

    }
    /*
      Funcion la cual intentara crear el archivo en donde se guardara el archivo JSON
      con toda la informacion del objeto tipo Noticia para su futuro uso
    */
    public function createFile($JSON){

      try {

        $this->setFileName('json');

        file_put_contents($this->fileFolder . $this->fileName , $JSON);

        if($this->saveFile()){

          return "El archivo ha sido creado";

        }
        else{

          return "Ha ocurrido un error con la Base de datos";

        }

      }catch(Exception $e) {

        return "No se ha podido crear el archivo";

      }

    }
    /*
     *
     *
     *
     */
     public function setImageDB($ImageRoute){

       $pdo = new Conexion();

       try{

         $query = $pdo->prepare("INSERT INTO imagenes (idnoticia , img_ruta) VALUES (:noticia , :ruta)");

         $query->bindParam(':noticia' , $this->newsId);

         $query->bindParam(':ruta' , $ImageRoute);

         $query->execute();

         return true;

       }
       catch(PDOExeption $e){

         return false;

       }

     }
     /*
      *
      *
      *
      */
      public function newPost(){

        $con = new Conexion();

        $con->query("UPDATE temp_noticia SET estado = 3 WHERE id_temp = " . $this->newsId);

        try{

          $getData = $con->prepare("SELECT * FROM temp_noticia WHERE id_temp = :id");

          $getData->bindParam(':id' , $this->newsId);

          $getData->execute();

          $temp = $getData->fetch(PDO::FETCH_ASSOC);

            if(count($temp) > 0){

              $setData = $con->prepare("INSERT INTO noticia (autor, fec_emis , titulo , subtitulo , archivo)
                                        VALUES(:autor , :fecha_creacion , :titulo , :subtitulo , :archivo)");

              $setData->bindParam(':autor' , $temp['autor']);

              $setData->bindParam(':fecha_creacion' , $temp['fecha_creacion']);

              $setData->bindParam(':titulo' , $temp['titulo']);

              $setData->bindParam(':subtitulo' , $temp['subtitulo']);

              $setData->bindParam(':archivo' , $temp['archivo']);

              $setData->execute();

              $this->newsId = $con->lastInsertId();

                return true;

            }else{

                return false;

            }
        }catch(PDOExeption $e){

            return false;

        }
      }
      /**
       * Funcion para guardar los subscriptores en base de datos
       */
      public function addSub($correo_subscriptor){

        $pdo = new Conexion(); 

            try{

              $query = $pdo->prepare("INSERT INTO subscriptor (correo_electronico) VALUES(:email)");
      
              $query->bindParam(':email' , $correo_subscriptor );
      
              $query->execute();
    
              return 1 ;
    
            }
            catch(PDOExeption $e){
    
              return 0 ;
    
            }
        
       
      }

      public function checkSub($correo_subscriptor){
        $pdo = new Conexion();
        $query = "SELECT COUNT(*) AS TOTALROWS FROM subscriptor WHERE correo_electronico = '".$correo_subscriptor."'";
        $sql = $pdo->query($query);
        $count = $sql->fetch(PDO::FETCH_ASSOC);
        $num_rows = $count['TOTALROWS'];

        return $num_rows;
      }

  }
 ?>
