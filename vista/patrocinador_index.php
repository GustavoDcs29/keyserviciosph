<?php
    require_once '../modelo/Conexion.php';
    $pdo = new Conexion();
?>
<!-- Bread crumb -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-primary">Publicidad</h3> </div>
    <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item active">Publicidad</li>
        </ol>
    </div>
</div>
<!-- End Bread crumb -->
<!-- Container fluid  -->
<div class="container-fluid">
    <!-- Start Page Content -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                  <div id="table_publicidad" class="row">
                    <div class="card-title">
                      <div class="col-md-6 col-xs-12" align="left">
                          <button class="btn btn-primary" onclick="loadContent('../vista/patrocinador_form.php')"><i class="fa fa-plus-circle fa-fw"></i> Nueva Publicidad</button>
                      </div>
                      </div>
                      <div class="col-md-6 col-xs-12" align="right">
                          <form>
                              <div class="input-group input-group-rounded" align="right">
                                  <input class="isearch form-control search" type="text" placeholder=" Buscar Publicidad">
                                  <span class="input-group-btn"><span class="btn btn-primary btn-group-right"><i class="ti-search"></i></span></span>
                              </div>
                          </form>
                      </div>

                      <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <th align="center">Cliente</th>
                                    <th align="center">Fecha de ingreso</th>
                                    <th align="center">Fecha de expiración</th>
                                    <th align="center">Status</th>
                                    <th align="center">Imagen</th>
                                    <th align="center">Acción</th>
                                </thead>
                                <tbody class="list">
                                <?php

                                    $query = "SELECT * FROM publicidad";
                                    $sql = $pdo->query($query);

                                    while( $result = $sql->fetch(PDO::FETCH_ASSOC) ){
                                        echo "<tr>
                                                <td class='nombre'>".$result['nombre_pu']."</td>
                                                <td class='fechai'>".$result['date_i']."</td>
                                                <td class='fechaf'>".$result['date_f']."</td>";

                                            if($result['status'] == 0){echo "<td><span class='status btn-sm btn-danger'>Inactivo</span></td>";}
                                            if($result['status'] == 1){echo "<td><span class='status btn-sm btn-success'>Activo</span></td>";}

                                        echo   "<td><img class='img-round' src='../".$result['direccion_img']."'/></td>
                                                <td>
                                                    <div class='dropdown'>
                                                        <button class='btn btn-primary dropdown-toggle' type='button' id='dropdownMenu1' data-toggle='dropdown' aria-haspopup='true' aria-expanded='true'>
                                                            Opciones
                                                            <span class='fa fa-cog' aria-hidden='true'></span>
                                                        </button>
                                                        <ul class='dropdown-menu' aria-labelledby='dropdownMenu1'>
                                                            <li onclick='changeStatus(".$result['id_publicidad'].",".$result['status'].")'>
                                                                <a href='#'>";

                                        if($result['status'] == 0){echo "Activar <i class='fa fa-flag-o' aria-hidden='true'></i>";}
                                        if($result['status'] == 1){echo "Desactivar <i class='fa fa-flag' aria-hidden='true'></i>";}

                                        echo                    "</a>
                                                            </li>
                                                            <li >
                                                                <a href='#'>
                                                                    Renovar <i class='fa fa-calendar' aria-hidden='true'></i>
                                                                </a>
                                                            </li>
                                                            <li onclick='deletePublicidad(".$result['id_publicidad'].")'>
                                                                <a href='#'>
                                                                    Eliminar <i class='fa fa-trash-o' aria-hidden='true'></i>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </td>
                                              </tr>";
                                    }
                                ?>
                                </tbody>
                            </table>
                        </div>
                          <ul class="pagination"></ul>
                      </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End PAge Content -->
</div>
<style>
    .img-round{
        height:80px;
        width:80px;
        border-radius:20px;
    }
</style>
<script type="text/javascript" src="../assets/js/module_publicidad.js"></script>
<script type="text/javascript" src="../assets/js/list.min.js"></script>
<script type="text/javascript">
    var optionws = {
        valueNames:["nombre","fechai","fechaf","status"],
        pagination: true,
        page:5
    };

    var list = new List("table_publicidad", optionws);
</script>
