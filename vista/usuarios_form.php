<!-- Bread crumb -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-primary">Usuarios</h3> </div>
    <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item active">Usuarios</li>
            <li class="breadcrumb-item active">Nuevo Usuario</li>
        </ol>
    </div>
</div>
<!-- End Bread crumb -->
<!-- Container fluid  -->
<div class="container-fluid">
    <!-- Start Page Content -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <h3 class="box-title">
                                Nuevo Usuario
                            </h3>
                        </div>
                        <div class="col-md-6" align="right">
                            <button class="cancel btn-danger" onclick="loadContent('../vista/usuarios_index.php')" ><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <form onsubmit="return false;" id="form_usuario">
                        <div class="row">
                            <div class="col-md-12 col-xs-12">
                                <label for="">Nombre y Apellido:</label> <br>
                                <label></label>
                                <input class="form-control" type="text" name="nomape"  />
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-6 col-xs-12">
                                <label for="">DNI:</label> <br>
                                <label></label>
                                <input class="form-control" type="text" name="dni"  />
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <label for=""> Nombre de Usuario:</label> <br>
                                <label></label>
                                <input class="form-control" type="text" name="username"  />
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-6 col-xs-12">
                                <label for="">Contraseña:</label> <br>
                                <label></label>
                                <input id="pass1" class="form-control" type="password" name="pass1"  />
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <label for="">Repetir Contraseña:</label> <br>
                                <label></label>
                                <input id="pass2" class="form-control" type="password" name="pass2"  />
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-12 col-xs-12" align="right">
                                <button class="btn btn-lg btn-success"><i class="fa fa-user-plus"></i> Agregar Usuario</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- End PAge Content -->
</div>
<script type="text/javascript" src="../assets/js/module_usuarios.js"></script>
<link rel="stylesheet" href="../assets/css/form.style.css" />
