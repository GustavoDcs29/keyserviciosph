<!-- Bread crumb -->
<link rel="stylesheet" href="css/lib/html5-editor/bootstrap-wysihtml5.css" />
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-primary">Noticias</h3> </div>
    <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item active">Noticias</li>
            <li class="breadcrumb-item active">Publicaciones</li>
            <li class="breadcrumb-item active">Nueva Publicación</li>
        </ol>
    </div>
</div>
<!-- End Bread crumb -->
<!-- Container fluid  -->
<div class="container-fluid">
    <!-- Start Page Content -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <h3 class="box-title">
                                Nueva Publicidad
                            </h3>
                        </div>
                        <div class="col-md-6" align="right">
                            <button class="cancel btn-danger" onclick="loadContent('../vista/blog_index.php')" ><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="wizard">
                            <div class="wizard-inner">
                                <!--<div class="connecting-line"></div>
                                <ul class="nav nav-tabs" role="tablist" >
                                    <li role="presentation" class="active">
                                        <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Step 1">
                                            <span class="round-tab">
                                                <i class="fa fa-info"></i>
                                            </span>
                                        </a>
                                    </li>

                                    <li role="presentation" class="disabled">
                                        <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Step 2">
                                            <span class="round-tab">
                                                <i class="fa fa-file-image-o"></i>
                                            </span>
                                        </a>
                                    </li>

                                    <li role="presentation" class="disabled">
                                        <a href="#step3" data-toggle="tab" aria-controls="step3" role="tab" title="Complete">
                                            <span class="round-tab">
                                                <i class="fa fa-check"></i>
                                            </span>
                                        </a>
                                    </li>
                                </ul>-->
                                <div class="progress m-t-20" style="height:15px">
                                    <div class="progress-bar bg-success" style="width: 0%; height:15px;" role="progressbar">0%</div>
                                </div>
                            </div>

                            <div class="tab-content">
                                <div class="tab-pane active" role="tabpanel" id="step1">
                                    <h1> Información General </h1>
                                    <div class="step1">
                                        <form onsubmit="return false;" id="form_publicacion">
                                            <div class="row">
                                                <div class="col-md-12 col-xs-12">
                                                    <label for="">Titulo:</label> <br>
                                                    <label></label>
                                                    <input class="form-control" type="text" name="titulo"  />
                                                </div>
                                            </div>
                                            <br />
                                            <div class="row">
                                                <div class="col-md-12 col-xs-12">
                                                    <label for="">Subtitulo:</label> <br>
                                                    <label></label>
                                                    <input class="form-control" type="text" name="subtitulo"  />
                                                </div>
                                            </div>
                                            <br />
                                            <div class="row">
                                                <div class="col-md-12 col-xs-12">
                                                    <label for="">Autor:</label> <br>
                                                    <label></label>
                                                    <input class="form-control" type="text" name="autor"  />
                                                </div>
                                            </div>
                                            <br />

                                            <hr />
                                            <ul class="list-inline pull-right">
                                                <li><button id="addData" type="submit" class="btn btn-lg btn-primary next-step">Siguiente</button></li>
                                            </ul>
                                        </form>
                                    </div>
                                </div>

                                <div class="tab-pane" role="tabpanel" id="step2">
                                    <div class="step2">
                                        <div class="row">
                                            <div class="col-md-1"></div>
                                            <div class="col-md-10 col-xs-12">
                                                <label for="">Por favor inserte el contenido de la publicación:</label> <br>
                                                <label></label>
                                                
                                                  <textarea id="cuadro-texto" class="textarea_editor form-control" rows="15" placeholder="Enter text ..." style="height:450px"></textarea>
                                            </div>
                                            <div class="col-md-1"></div>
                                        </div>
                                        	<div class="row" style="margin-top:200px;">
                                            <div class="col-md-12"><hr /></div>
                                            <div class="col-md-12" align="right">
                                                <button id="uploadContent" type="button" class="btn btn-lg btn-primary next-step">Siguiente</button>
                                            </div>
                                          </div>


                                    </div>
                                </div>

                                <div class="tab-pane" role="tabpanel" id="step3">
                                    <div class="step3">
                                      <div class="row">
                                          <div class="col-md-1"></div>
                                          <div class="col-md-10  col-xs-12">
                                              <label for="">Favor seleccione la imagen que desea agregar:</label> <br>
                                              <label></label>
                                              <div id="myDropzone" class="dropzone row" align="center">
                                                  <div class="fallback">
                                                      <input name="file" type="file"  />
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="col-md-1"></div>
                                      </div>
                                        <br />
                                        <hr />
                                        <ul class="list-inline pull-right">
                                            <!--<li><button class="btn btn-danger"> Cancelar </button></li>-->
                                            <li><button id="uploadImage" type="button" class="btn btn-lg btn-primary next-step">Siguiente</button></li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="tab-pane" role="tabpanel" id="complete">
                                    <div class="step44">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-md-6 col-md-offset-3">
                                                    <h1 id="rsp-tittle"></h1>
                                                    <i id="rsp-icon" class="" aria-hidden="true"></i>
                                                    <label id="rsp-msj"></label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6 col-md-offset-3" align="right">
                                                    <button onclick="loadContent('../vista/blog_index.php')" class="btn btn-success">Salir</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                          </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
    <!-- End PAge Content -->
</div>
<link rel="stylesheet" href="../assets/css/form.style.css" />
<link href="../assets/css/dropzone.css" rel="stylesheet">
<link href="../assets/css/wizard2.css" rel="stylesheet">
<script type="text/javascript" src="../assets/js/dropzone.js"></script>
<script type="text/javascript" src="../assets/js/gwizard.js"></script>
<script type="text/javascript" src="../assets/js/configuracionQuill.js"></script>
<script type="text/javascript" src="../assets/js/module_blog.js"></script>
<script src="js/lib/html5-editor/wysihtml5-0.3.0.js"></script>
<script src="js/lib/html5-editor/bootstrap-wysihtml5.js"></script>
<script src="js/lib/html5-editor/wysihtml5-init.js"></script> 