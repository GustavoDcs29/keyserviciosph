<?php
    require_once '../modelo/Conexion.php';
    $pdo = new Conexion();
?>
<!-- Bread crumb -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-primary">Noticias</h3> </div>
    <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item">Noticias</li>
            <li class="breadcrumb-item active">Borradores</li>
        </ol>
    </div>
</div>
<!-- End Bread crumb -->
<!-- Container fluid  -->
<div class="container-fluid">
    <!-- Start Page Content -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                  <div id="table_publicaciones" class="row">
                    <div class="card-title">
                      <!--<div class="col-md-6 col-xs-12" align="left">
                          <button class="btn btn-primary" onclick="loadContent('../vista/blog_form.php')"><i class="fa fa-plus-circle fa-fw"></i> Nueva Publicación</button>
                      </div>-->
                    </div>
                      <div class="col-md-6 col-xs-12" align="right">
                          <form>
                              <div class="input-group input-group-rounded" align="right">
                                  <input class="isearch form-control search" type="text" placeholder=" Buscar Publicación">
                                  <span class="input-group-btn"><span class="btn btn-primary btn-group-right"><i class="ti-search"></i></span></span>
                              </div>
                          </form>
                      </div>

                      <div class="col-md-12">
                        <div class="table-responsive">
                          <table  class="table">
                              <thead>
                                  <th class="" align="center">Id</th>
                                  <th class="" align="center">Titulo</th>
                                  <th class="" align="center">Subtitulo</th>
                                  <th class="" align="center">Autor</th>
                                  <th class="" align="center">Emisión</th>
                                  <th class="" align="center">Acción</th>
                              </thead>
                              <tbody class="list">
                              <?php

                                  $query = "SELECT * FROM temp_noticia WHERE estado < 3";
                                  $sql = $pdo->query($query);

                                  while( $result = $sql->fetch(PDO::FETCH_ASSOC) ){
                                      //$editar = "cliente_form_update.php?id_cliente=".$result['id_cliente'];
                                      $url_con = '"../vista/blog_form.php?idb=' . $result['id_temp'] . '"' ;
                                      echo "<tr>
                                              <td class='id'>".$result['id_temp']."</td>
                                              <td class='titulo'>".$result['titulo']."</td>
                                              <td class='subtitulo'>".$result['subtitulo']."</td>
                                              <td class='autor'>".$result['autor']."</td>
                                              <td class='fec_emis'>".$result['fecha_creacion']."</td>
                                              <td>
                                                  <div class='dropdown'>
                                                      <button class='btn btn-primary dropdown-toggle' type='button' id='dropdownMenu1' data-toggle='dropdown' aria-haspopup='true' aria-expanded='true'>
                                                          Opciones
                                                          <span class='fa fa-cog' aria-hidden='true'></span>
                                                      </button>
                                                      <ul class='dropdown-menu' aria-labelledby='dropdownMenu1'>
                                                          <li>
                                                              <a onclick='loadFormBorrador(" . $result['id_temp'] . ")' href='#' >
                                                                  Continuar <i class='fa fa-pencil-square-o' aria-hidden='true'></i>
                                                              </a>
                                                          </li>
                                                          <li>
                                                              <a href='#'>
                                                                  Eliminar <i class='fa fa-trash-o' aria-hidden='true'></i>
                                                              </a>
                                                          </li>
                                                      </ul>
                                                  </div>
                                              </td>
                                            </tr>";
                                  }
                              ?>
                              </tbody>
                          </table>
                        </div>
                          <ul class="pagination"></ul>
                      </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End PAge Content -->
</div>
<script type="text/javascript" src="../assets/js/gwizard.js"></script>
<script type="text/javascript" src="../assets/js/module_blog.js"></script>
<script type="text/javascript" src="../assets/js/list.min.js"></script>
<script type="text/javascript">
    var options = {
        valueNames:["id","titulo","subtitulo","autor","fec_emis"],
        pagination: true,
        page:10
    };

    var publicacioneslist = new List("table_publicaciones",options);
</script>
