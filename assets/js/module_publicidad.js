    var sponsor = $('#npatrocinador');
    var dinicio = $('#finicio');
    var dfinal = $('#ffin');

$('#form_patrocinador').validate({
    errorElement: 'em',
    rules:{
        "npatrocinador":{
            required : true
        },
        "finicio":{
            required: true
        },
        "ffin":{
            required: true
        }
    },

    submitHandler: function(form){
        $.ajax({
        type:'POST',
        data: $('#form_patrocinador').serialize(),
        url: '../controlador/addTempAdver.php',
        dataType:'json',
        success: function(data){

            wizardSuccess('step1',50);

        },
        error: function(errorMessage){

            console.log(errorMessage);

        }
        });

        $('#form_patrocinador')[0].reset();
    }
});

   $('#myDropzone').dropzone({
        url: '../uploadPublicidad.php',
        autoProcessQueue: false,
        uploadMultiple: true,
        parallelUploads: 5,
        maXFile : 1,
        maxFilesize : 5, // 5MB
        acceptedFiles : 'image/png',
        addRemoveLinks : true,
        init : function() {
            dzClosure = this ;

            document.getElementById('SubmitLogo').addEventListener("click", function(e){
                e.preventDefault();
                e.stopPropagation();
                dzClosure.processQueue();
            });

            this.on("success", function(file ,response){

                wizardSuccess('step2',100);

                $('#rsp-tittle').html("Felicitaciones!");
                $('#rsp-icon').addClass('fa fa-check-circle-o');
                $('#rsp-msj').html(response);

            });

            this.on("error", function(file, errorMessage){

                console.log(errorMessage);

                wizardFailure('step2');

                $('#rsp-tittle').html("Ha ocurrido un error!");
                $('#rsp-icon').addClass('fa fa-ban');
                $('#rsp-msj').html(errorMessage);

            });
        }
    });

    /**
     * Funcion implementada para eliminar mediante ajax los datos y la imagen del servidor
     * @param {number} publicidad
     */
     function deletePublicidad(publicidad){
        var idPublicidad = "id_publicidad=" + publicidad;

        $.ajax({
            url:  '../controlador/deletePublicidad.php',
            type: 'POST',
            data: idPublicidad,
            dataType: 'JSON',
            success: function(data){
                swal(data.titulo, data.msj , data.icon);
                loadContent(data.content);
            },
            error: function(errorMessage){
                console.log(errorMessage);
            }
        });
    }

    /**
     * La funcion changeStatus es implementada para cambiar el status de los clientes que rentan el espacion
     * mediante ajax
     * en la pagina Web, la funcion revice dos paramentros los cuales son los siguientes.
     * @param {number} publicidad
     * @param {boolean} status
     */
    function changeStatus(publicidad,status){
        var datos = "id_publicidad=" + publicidad + "&status=" + status;

        $.ajax({
            url: '../controlador/changeStatusAdver.php',
            type: 'POST',
            data: datos,
            dataType: 'JSON',
            success: function(data){
                swal(data.titulo, data.msj , data.icon);
                loadContent(data.content);
            },
            error: function(error){
                console.log(error);
            }
        });
    }
