/**
 * Validate & Ajax Handler para el formulario de Agregar clientes
 */
  $('#form_cliente').validate({
      errorElement: 'em',
      rules:{
        "ncliente":{
          required : true
        },
        "codigo":{
          required: false
        },
        "telefono":{
          required: false,
          digits: true
        },
        "acceso": {
          required: true
        },
        "direccion":{
          required:false
        }
      },

      submitHandler: function(form){
        $.ajax({
          type:'POST',
          data: $('#form_cliente').serialize(),
          url: '../controlador/saveCliente.php',
          dataType:'json',
          success: function(data){
            swal(data.titulo, data.msj , data.icon);
            loadContent(data.content);
          },
          error: function(){
            console.log('Error!');
          }
        });

        $('#form_cliente')[0].reset();
      }
  });

/**
 * Function deleteCliente elimina al cliente de la base de datos
 * utiliza su identificador unico para eliminarlo por ajax y luego carga
 * la pagina de Index de los cliente.
 * @param {number} id_cliente
 */
  function deleteCliente(id_cliente){

    var Cliente = "id_cliente=" + id_cliente ;

    $.ajax({
      type: 'POST',
      data: Cliente,
      url: '../controlador/deleteCliente.php',
      dataType: 'json',
      success:function(data){
        swal(data.titulo, data.msj , data.icon);
        loadContent(data.content);
      },
      error:function(data){
        console.log("Error!");
      }
    });
  }

/**
 * Function editCliente es para llamar los datos del cliente por ajax
 * y pasarlos a los input del formulario para ser editados
 * @param {number} id_cliente
 */
  function editCliente(id_cliente){
    var Cliente = "id_cliente=" + id_cliente ;

    loadContent("../vista/cliente_form_update.php");

    $.ajax({
      type: 'POST',
      data: Cliente,
      url: '../controlador/dataCliente.php',
      dataType: 'JSON',
      error: function(data){
        alert('Error!');
      }
    })
    .done(function (data){
      fillData(data);
    });
  }

/**
 * Function fillData es una funcion que se usara para llenar los campos input, textarea
 * del formulario de EDITAR CLIENTES usando javascript
 * @param {JSON} json
 */

 function fillData(json){
   $.each(json ,function(key,value){
      $('#' + key).val(value);
   });
 }

/**
 * Validate & Ajax Handler para el formulario de Editar clientes
 * var form = document.getElementbyId('form_cliente_up') ;
 *
 */
$('#form_cliente_up').validate({
  errorElement: 'em',
  rules:{
    "ncliente":{
      required : true
    },
    "codigo":{
      required: false
    },
    "telefono":{
      required: false,
      digits: true
    },
    "acceso": {
      required: true
    },
    "direccion":{
      required:false
    }
  },

  submitHandler: function(form){
    $.ajax({
      type:'POST',
      data: $('#form_cliente_up').serialize(),
      url: '../controlador/updateCliente.php',
      dataType:'json',
      success: function(data){
        swal(data.titulo, data.msj , data.icon);
        loadContent(data.content);
      },
      error: function(){
        alert('Error!');
      }
    });

    $('#form_cliente_up')[0].reset();
  }
});
