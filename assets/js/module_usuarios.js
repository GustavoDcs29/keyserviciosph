/**
 * Validate & Ajax Handler para el formulario de Agregar Usuarios
 */
$('#form_usuario').validate({
    rules:{
      "nomape":{
        required : true
      },
      "dni":{
        required: true
      },
      "username":{
        required: true
      },
      "pass1": {
        required: true        
      },
      "pass2":{
        required:true,
        equalTo: "#pass1"
      }
    },

    submitHandler: function(form){
      $.ajax({
        type:'POST',
        data: $('#form_usuario').serialize(),
        url: '../controlador/saveUser.php',
        dataType:'json',
        success: function(data){
          swal(data.titulo, data.msj , data.icon);
          loadContent(data.content);
        },
        error: function(){
          console.log('Error!');
        }
      });

      $('#form_usuario')[0].reset();
    }
});

/**
 * Mensajes de error de la validacion 
 */
jQuery.extend(jQuery.validator.messages, {
    required: "Este campo es obligatorio",
    email: "Escriba una dirección de correo válida",
    digits: "Escriba sólo números",
    equalTo: "Las contraseñas no coinciden, por favor ingreselas de nuevo"
});

/**
 * Function deleteUser elimina al usuario de la base de datos
 * utiliza su identificador unico para eliminarlo por ajax y luego carga
 * la pagina de Index de los usuario.
 * @param {number} id_usuario 
 */
function deleteUser(id_usuario){
  
      var Usuario = "id_usuario=" + id_usuario ;
  
      $.ajax({
        type: 'POST',
        data: Usuario,
        url: '../controlador/deleteUser.php',
        dataType: 'json',
        success:function(data){
          swal(data.titulo, data.msj , data.icon);
          loadContent(data.content);
        },
        error:function(data){
          console.log("Error!");
        }
      });
}

/**
 * Function editUser es para llamar los datos del usuario por ajax
 * y pasarlos a los input del formulario para ser editados
 * @param {number} id_user
 */
function editUser(id_user){
  var User = "id_user=" + id_user ;

  loadContent("../vista/usuarios_form_update.php");

  $.ajax({
    type: 'POST',
    data: User,
    url: '../controlador/dataUser.php',
    dataType: 'JSON',
    error: function(data){
      alert('Error!');
    }
  })
  .done(function (data){
    fillData(data);
  });
}

/**
* Function fillData es una funcion que se usara para llenar los campos input, textarea 
* del formulario de EDITAR CLIENTES usando javascript
* @param {JSON} json
*/

function fillData(json){
 $.each(json ,function(key,value){
    $('#' + key).val(value);
 });
}

/**
 * Validate & Ajax Handler para el formulario de Editar Usuarios
 */
$('#form_usuario_up').validate({
  rules:{
    "nomape":{
      required : true
    },
    "dni":{
      required: true
    },
    "username":{
      required: true
    },
    "pass1": {
      required: false        
    },
    "pass2":{
      required:false,
      equalTo: "#pass1"
    }
  },

  submitHandler: function(form){
    $.ajax({
      type:'POST',
      data: $('#form_usuario_up').serialize(),
      url: '../controlador/updateUser.php',
      dataType:'json',
      success: function(data){
        swal(data.titulo, data.msj , data.icon);
        loadContent(data.content);
      },
      error: function(){
        console.log('Error!');
      }
    });

    $('#form_usuario_up')[0].reset();
  }
});
