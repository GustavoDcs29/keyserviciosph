$(document).ready(function () {

    $('#myCarousel').carousel({
      pause: 'none'
    });

    $('#myCarouse2').carousel({
        interval: 10000
    });

    $('.fdi-Carousel .item').each(function () {
        var next = $(this).next();
		if (!next.length) {
            next = $(this).siblings(':first');
        }
        next.children(':first-child').clone().appendTo($(this));

		var next2 = next.next();
		if (!next2.length) {
            next2 = $(this).siblings(':first');
        }
		next2.children(':first-child').clone().appendTo($(this));

		var next3 = next2.next();
		if(!next3.length){
			next3 = $(this).siblings(':first');
		}
		next3.children(':first-child').clone().appendTo($(this));
    });

    $('#Contacto').validate({
      rules:{
        "name":{
          required : true
        },
        "email":{
          required: true,
          email: true
        },
        "telephone":{
          required: true,
          digits: true
        },
        "message": {
          required: true
        }
      },

      submitHandler: function(form){
        $.ajax({
          type:'POST',
          data: $('#Contacto').serialize(),
          url: 'controlador/sendEmail.php',
          success: function(data){
            swal(data);
          },
          error: function(data){
            swal(data);
          }
        });

        $('#Contacto')[0].reset();
      }
    });

    jQuery.extend(jQuery.validator.messages, {
      required: "Este campo es obligatorio",
      email: "Escriba una dirección de correo válida",
      digits: "Escriba sólo números",
      equalTo: "Las contraseñas no coinciden, por favo ingreselas de nuevo"
    });

    var widthWindow = $(window).width();

    if( widthWindow < 768){
        $('div#body').removeClass('container');
    }


});

/**
 * La funcion seleccion es usada para permitir el acceso a los clientes a su pagina
 * de acceso a su sistema correspondinte.
 */
  function seleccion(){
    var seleccion = "id_acceso=" + $('#seleccionPh').val();

    $.ajax({
      type: 'POST',
      data: seleccion,
      url: 'controlador/openpage.php',
      success: function(direccion){
        location.replace( "https://" + direccion );
      },
      error: function(error){
        alert("Ha ocurrido un error! Por favor seleccione un Ph.");
        console.log(error);
      }
    });
  }

/**
 * La funcion loadContent se usa para cargar el contenido en el contenedor 
 * especifico #Page-Content en la User Interface
 * @param {String} url direccion del archivo a cargar en el contenedor
 */
  function loadContent(url){
    var contenido = url ;
    $('#Page-Content').load(contenido);
  }

/**
 * Access comprueba el user y pass ingresado con la base de datos para permitir el acceso al sistema
 * implementado ajax
 */
  function Access(){

    $('#login').validate({
      rules:{
        "username":{
          required : true
        },
        "password":{
          required: true,
        }
      },

      submitHandler: function(form){
        $.blockUI({ 
          message: '<h3>Por favor, espere!</h3><img src="../assets/img/Infinity.gif" />' ,
          css: { backgroundColor: '#fff', color: '#000'}
        });
        $(document).ajaxStop($.unblockUI);
        $.ajax({
          type: 'POST',
          data: $("#login").serialize(),
          url: '../controlador/accessUser.php',
          success: function(rsp){
            if(rsp === 404){

              swal("Usuario o contrasena errada!");
            }
            if(rsp === "dashboard.php"){
              window.open(rsp, "_self");
            }
          },
          error: function(rsp){
            
            swal("Ha ocurrido un error!");
          }
        });
        $('#login')[0].reset();
      }
    });

    jQuery.extend(jQuery.validator.messages, {
      required: "Este campo es obligatorio"
    });
  }
