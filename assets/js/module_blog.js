/*
 * Step 1
 */
$('#form_publicacion').validate({
    errorElement: 'em',
    rules:{
        "titulo":{
            required : true
        },
        "subtitulo":{
            required: true
        },
        "autor":{
            required: true
        }
    },

    submitHandler: function(form){
        $.ajax({
        type:'POST',
        data: $('#form_publicacion').serialize(),
        url: '../controlador/addBorradorPost.php',
        success: function(data){
          if(data == true){

            wizardSuccess('step1',33);

          }
          else{

            alert("Ha ocurrido un error");

            console.log(data);

          }
        },
        error: function(data){
            console.log('Error!');
            console.log(data);
        }
        });

        $('#form_publicacion')[0].reset();
    }
});
/*
 * Step 2
 */
$('#uploadContent').on('click', function(){
  var delta = quill.getContents();

  $.ajax({
    data: {myContent:delta.ops},
    url: '../controlador/saveContent.php',
    type: 'POST',
    success: function(rsp){

      wizardSuccess('step2',67);

    },
    error: function(e){

      console.log(e);

    }
  });
})
/*
 * Step 3
 */
$('#myDropzone').dropzone({
     url: '../uploadImage.php',
     autoProcessQueue: false,
     uploadMultiple: true,
     parallelUploads: 5,
     maXFile : 2,
     maxFilesize : 5, // 5MB
     acceptedFiles : 'image/*',
     addRemoveLinks : true,
     init : function() {
         dzClosure = this ;

         document.getElementById('uploadImage').addEventListener("click", function(e){
             e.preventDefault();
             e.stopPropagation();
             dzClosure.processQueue();
         });

         this.on("success", function(file ,response){

             console.log(response);

             wizardSuccess('step3',100);

             $('#rsp-tittle').html("Felicitaciones!");
             $('#rsp-icon').addClass('fa fa-check-circle-o');
             $('#rsp-msj').html(response);

         });

         this.on("error", function(file, errorMessage){

             console.log(errorMessage);

             wizardFailure('step3');

             $('#rsp-tittle').html("Ha ocurrido un error!");
             $('#rsp-icon').addClass('fa fa-ban');
             $('#rsp-msj').html(errorMessage);

         });
     }
 });

 /**
  * 
  */
 function loadFormBorrador(idBorrador){ 

    var datos = "idBorrador=" + idBorrador ; 
    loadContent("../vista/blog_form.php");
    $.ajax({
        type: 'POST',
        data: datos,
        url: '../controlador/retrieveBorradorData.php',
        dataType: 'JSON',
        success: function(data){
            /*console.log(data);*/

            

            var step = "step" + data.estado ;
            var value ;
            if(step === "step2"){ value = 33 ;}
            if(step === "step3"){ value = 66 ;}

            wizardSuccess( step , value );

            
        },
        error: function(error){
            console.log(error);
        }
    });

 }
