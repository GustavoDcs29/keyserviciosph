<?php
  $numeros = array(10,51,8,4,6,69,12,1,36,7,9,0,6,5,15);

  function checkNumber($arregloNumeros){
    //Variables a implementar en la funcion
    $promedio = 0 ;
    $numMayor = null ;
    $numMenor = null ;
    $totPares = 0 ;
    $totImpar = 0 ;
    $total = 0 ;
    $numArray = count($arregloNumeros);

    //Recorrer el arreglo de numeros
    foreach ($arregloNumeros as $numero) {
      //Sumatoria del total de los elementos en el arreglo
      $total = $total + $numero ;
      //Se inicializan los numeros
      if($numMayor == null && $numMenor == null){
        $numMayor = $numero ;
        $numMenor = $numero ;
      }
      //De ya estar inicializados se procede a verificar
      else{
        //Si el elemento es mayor a la bandera numero mayor, este sera el nuevo numero mayor
        if($numero > $numMayor){
          $numMayor = $numero;
        }
        //De caso que no sea mayor
        else{
          //El numero nuevo se compara con la bandera, de ser menor a la bandera, sera el nuevo numero menor
          if($numero <= $numMenor){
            $numMenor = $numero ;
          }
        }
      }
      //Se determina si el numero es par calculando el valor del resto de la division si el valor es
      //cero entonces el numero es par
      if($numero%2 == 0){
        $totPares++;
      }
      //De caso contrario el numero seria impar
      else{
        $totImpar++;
      }
    }
    //Se calcula el promedio del total general dentro del arreglo de numero
    //dividiendolo entre el total de elementos en el arreglo
    $promedio = $total / $numArray ;

    $resultado = "El promedio es: " . $promedio ;
    $resultado .= "<br> El numero mayor es: " . $numMayor ;
    $resultado .= "<br> El numero menor es: " . $numMenor ;
    $resultado .= "<br> El total de numero impares es : " . $totImpar ;
    $resultado .= "<br> El total de numero pares es: " . $totPares ;

    print($resultado);

  }

  checkNumber($numeros);
?>
