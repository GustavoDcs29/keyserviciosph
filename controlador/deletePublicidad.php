<?php
    require '../modelo/Conexion.php';
    $idPublicidad = $_POST['id_publicidad'];
    $pdo = new Conexion();

    try{
        $query = $pdo->prepare("SELECT direccion_img FROM publicidad WHERE id_publicidad = :id");
        $query->bindParam(':id' , $idPublicidad);
        $query->execute();
        $result = $query->fetch(PDO::FETCH_ASSOC);
        $direccion = "../".$result['direccion_img'];

        $deleted  = unlink($direccion);

            if($deleted == true){
                try{
                    $query2 = $pdo->prepare("DELETE FROM publicidad WHERE id_publicidad = :id2");
                    $query2->bindParam('id2', $idPublicidad);
                    $query2->execute();

                    $res = new stdClass();
                    $res->icon = "success";
                    $res->titulo = "Operacion Exitosa";
                    $res->msj = "Publicidad eliminada de forma exitosa";
                    $res->btn = "#66BB6A";	
                    $res->content = "../vista/patrocinador_index.php";		
                    $json = json_encode($res);

                    echo $json;

                }catch(PDOExeption $e){

                    $res = new stdClass();
                    $res->icon = "warning";
                    $res->titulo = "Falla al borrar en BD";
                    $res->msj = "Ha ocurrido un error, por favor comunicarse con el administrador del sistema!";
                    $res->btn = "#EF5350";	
                    $res->content = "../vista/patrocinador_index.php";		
                    $json = json_encode($res);
                    
                    echo $json;

                }

            }else{
                $res = new stdClass();
                $res->icon = "warning";
                $res->titulo = "Falla al borrar la Imagen";
                $res->msj = "Ha ocurrido un error, por favor comunicarse con el administrador del sistema!";
                $res->btn = "#EF5350";	
                $res->content = "../vista/patrocinador_index.php";		
                $json = json_encode($res);
                
                echo $json;
            }
    }catch(PDOExeption $e){
        
        $res = new stdClass();
        $res->icon = "warning";
        $res->titulo = "Ha ocurrio un error al Conectarse con la BD";
        $res->msj = "Ha ocurrido un error, por favor comunicarse con el administrador del sistema!";
        $res->btn = "#EF5350";	
        $res->content = "../vista/patrocinador_index.php";		
        $json = json_encode($res);

        echo $json;
    }
    
?>