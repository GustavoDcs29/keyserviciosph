<?php
  session_name('temp_noticias');
  session_start();

  require '../modelo/Noticia.php';
  $news = new Noticia($_SESSION['LastId'] , $_SESSION['Titulo']);

  if(isset($_POST['myContent'])){

    $JSON = json_encode($_POST['myContent']);

    $mensaje = $news->createFile($JSON);

    $arreglo = array('resultado' => $mensaje);

    $rsp = json_encode($arreglo);

  }
  else{

    $arreglo = array('error' => "Ha ocurrido un error");

    $rsp = json_encode($arreglo);

  }

    echo $rsp;

?>
