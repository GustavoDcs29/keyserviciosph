<?php
    require '../modelo/Conexion.php';
    $pdo = new Conexion();

    $usuario = $_POST['id_user'];

    try{
        $query = $pdo->prepare("SELECT * from usuarios WHERE id_user = :value");
        $query->bindParam(":value" , $usuario );
        $query->execute();

        $result = $query->fetch(PDO::FETCH_ASSOC);

        $rsp = new stdClass();
        $rsp->id_user = $result["id_user"];
        $rsp->nomape = $result["nomape"];
        $rsp->dni = $result["dni"];
        $rsp->username = $result["username"];

        $json = json_encode($rsp);
        echo $json;
        
    }
    catch(PDOExeption $e){
        echo "ERROR";
    }




?>