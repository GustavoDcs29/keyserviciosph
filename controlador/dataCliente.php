<?php
    require '../modelo/Conexion.php';
    $pdo = new Conexion();

    $cliente = $_POST['id_cliente'];

    try{
        $query = $pdo->prepare("SELECT * from clientes WHERE id_cliente = :value");
        $query->bindParam(":value" , $cliente );
        $query->execute();

        $result = $query->fetch(PDO::FETCH_ASSOC);

        $rsp = new stdClass();
        $rsp->id_cliente = $result["id_cliente"];
        $rsp->ncliente = $result["nombre_cliente"];
        $rsp->codigo = $result["nit"];
        $rsp->direccion = $result["direccion"];
        $rsp->telefono = $result["telefono"];
        $rsp->acceso = $result["url_sistema"];

        $json = json_encode($rsp);
        echo $json;
        
    }
    catch(PDOExeption $e){
        echo "ERROR";
    }




?>