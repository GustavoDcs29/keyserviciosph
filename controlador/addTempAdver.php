<?php
    session_name('temp');
    session_start();

    require '../modelo/Conexion.php';
    $pdo = new Conexion();
    
    try{

        $query = $pdo->prepare("INSERT INTO temp_publicidad (name_t , dates_t , datef_t)
        VALUES (:nombre, :dates, :datef)");
    
        $query->bindParam(':nombre',$_POST['npatrocinador']);
        $query->bindParam(':dates',$_POST['finicio']);
        $query->bindParam(':datef',$_POST['ffin']);

    
        $query->execute();

        $id = $pdo->lastInsertId();
        $_SESSION['LastId'] = $id; 
        
        echo true;

    }catch(PDOException $e){

        echo $e ; 

    }
    

?>