<?php

    require '../modelo/Conexion.php';
    $pdo = new Conexion();

    $id_cliente = $_POST['id_cliente'];

    $query = $pdo->prepare("DELETE FROM clientes WHERE id_cliente = :cliente ");

    try{

        $query->bindParam(':cliente' , $id_cliente);
        $sql = $query->execute();

        $res = new stdClass();
        $res->icon = "success";
        $res->titulo = "Operacion Exitosa";
        $res->msj = "Cliente eliminado de forma exitosa";
        $res->btn = "#66BB6A";	
        $res->content = "../vista/cliente_index.php";		
        $json = json_encode($res);
        echo $json;


    }catch(PDOException $e){

        $res = new stdClass();
        $res->icon = "warning";
        $res->titulo = "Falla al borrar en BD";
        $res->msj = "Ha ocurrido un error, por favor comunicarse con el administrador del sistema!";
        $res->btn = "#EF5350";	
        $res->content = "../vista/cliente_index.php";		
        $json = json_encode($res);
        echo $json;

    }



?>