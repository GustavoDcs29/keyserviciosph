<?php
    require_once '../modelo/Contact.php';

    $mail = new Contact($_POST);

    $mail->setTitle("Servicio Automatizado de Contacto Key's Soluciones PH");
    $mail->setHeader();
    $mail->setContent();
    $result = $mail->Send();

    if($result == true){
        echo "Su Correo ha sido enviado de forma exitosa! Gracias por comunicarse con nosotros!";
    }
    else{
        echo "Ha ocurrido un error con el envio, por favor intentelo de nuevo!";
    }

    
?>