<?php
    session_name('Keys');
    session_start();
    if(!isset($_SESSION['usuario'])){
        header("Location: index.php");
        exit;
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Dashboard Key's</title>
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="../assets/css/sweetalert.css" />
    <!--<script src='https://cloud.tinymce.com/stable/tinymce.min.js'></script>-->
    <script src="//cdn.quilljs.com/1.3.6/quill.min.js"></script>
    <link href="//cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
    <style>
    input.isearch{
        border: 1px solid lightblue;
        border-radius: 20px;
        height:30px;
        width:250px;
        padding:20px;
    }
    </style>

    <!-- color CSS -->
    <link href="css/colors/default.css" id="theme" rel="stylesheet">
</head>

<body class="fix-header">
    <!-- ============================================================== -->
    <!-- Preloader -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
        </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Wrapper -->
    <!-- ============================================================== -->
    <div id="wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <nav class="navbar navbar-default navbar-static-top m-b-0">
            <div class="navbar-header">
                <div class="top-left-part">
                    <!-- Logo -->
                    <a class="logo" href="index.php">
                        <!-- Logo icon image, you can use font-icon also --><b>
                        <!--This is dark logo icon-->
                        <img src="../../assets/img/LOGOKEY2.png" alt="home" class="dark-logo" />
                        <!--This is light logo icon
                        <img src="../plugins/images/admin-logo-dark.png" alt="home" class="light-logo" />-->
                     </b>
                        <!-- Logo text image you can use text also -->
                    <span class="hidden-xs">
                        <!--This is dark logo text-->
                        <img src="../../assets/img/LOGOKEY2.png" alt="home" class="dark-logo" />
                        <!--This is light logo text
                        <img src="../plugins/images/admin-text-dark.png" alt="home" class="light-logo" />-->
                    </span> </a>
                </div>
                <!-- /Logo -->
                <ul class="nav navbar-top-links navbar-right pull-right">
                    <li>
                        <a class="profile-pic" href="#">
                            <i class="fa fa-user" aria-hidden="true" style="margin-right:3px"></i>
                            <b class="hidden-xs">  <?=$_SESSION['nomape'] ?>    </b>
                        </a>
                    </li>
                    <li>
                        <a class="profile-pic" href="cerrar.php">
                            <i class="fa fa-sign-out" aria-hidden="true" style="margin-right:3px"></i>
                            <b class="hidden-xs">Cerrar Sesión</b>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-header -->
            <!-- /.navbar-top-links -->
            <!-- /.navbar-static-side -->
        </nav>
        <!-- End Top Navigation -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav">
                <div class="sidebar-head">
                    <h3>
                        <span class="fa-fw open-close"><i class="ti-close ti-menu"></i></span>
                        <span class="hide-menu">Navigation</span>
                    </h3>
                </div>
                <div class="navbar-collapse" >
                <ul class="nav" id="side-menu">
                    <li style="padding: 70px 0 0;">
                        <a onclick="loadContent('../vista/main.php')" class="waves-effect">
                            <i class="fa fa-home fa-fw" aria-hidden="true"></i>Inicio
                        </a>
                    </li>
                    <li>
                        <a onclick="loadContent('../vista/blog_index.php')" class="waves-effect">
                            <i class="fa fa-align-left fa-fw" aria-hidden="true"></i> Blog
                        </a>
                    </li>
                    <li>
                        <a onclick="loadContent('../vista/cliente_index.php')" class="waves-effect">
                            <i class="fa fa-users fa-fw" aria-hidden="true"></i> Clientes
                        </a>
                    </li>
                    <li>
                        <a onclick="loadContent('../vista/patrocinador_index.php')" class="waves-effect">
                            <i class="fa fa-file-image-o fa-fw" aria-hidden="true"></i> Publicidad
                        </a>
                    </li>
                    <li>
                        <a onclick="loadContent('../vista/usuarios_index.php')" class="waves-effect">
                            <i class="fa fa-cogs fa-fw" aria-hidden="true"></i> Usuarios
                        </a>
                    </li>
                    <li>
                        <a href="cerrar.php" class="waves-effect">
                            <i class="fa fa-power-off fa-fw" aria-hidden="true"></i> Cerrar Sesión
                        </a>
                    </li>
                </ul>
                </div>
            </div>

        </div>
        <!-- ============================================================== -->
        <!-- End Left Sidebar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page Content -->
        <!-- ============================================================== -->
        <div id="page-wrapper">
            <div id="Page-Content">
                <div class="container-fluid">
                    <div class="row bg-title">
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                            <h4 class="page-title"> Inicio </h4>
                        </div>
                        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                            <ol class="breadcrumb">
                                <li><a href="#">Inicio</a></li>
                            </ol>
                        </div>
                    </div>
                    <div class="row" style="margin-botton:10px;">
                        <div class="row" align="center">
                            <img height="100%" width="100%" src="../assets/img/logodash.jpg" />
                        </div>
                    </div>
                </div>
            </div>
            <footer class="footer text-center"> 2017 &copy; Key's Servicios y Soluciones S.A. </footer>
        </div>
        <!-- ============================================================== -->
        <!-- End Page Content -->
        <!-- ============================================================== -->
    </div>
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="../plugins/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="../assets/js/bootstrap.min.js"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
    <!--slimscroll JavaScript -->
    <script src="js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="js/waves.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="js/custom.min.js"></script>
    <script src="../assets/js/validate.min.js"></script>
    <script src="../assets/js/functions.js"></script>
    <script src="../assets/js/sweetalert.min.js" ></script>
</body>

</html>
