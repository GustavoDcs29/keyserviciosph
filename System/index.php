<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title> Key's Servicios y Soluciones </title>
        <meta name="description" content="Key's servicios y soluciones, administracion de propiedades horizontales" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <meta name="generator" content="Codeply">
        <link rel="shortcut icon" type="image/x-icon" href="../assets/img/favicon.png" >
        <link rel="stylesheet" href="../assets/css/bootstrap.min.css" />
        <link rel="stylesheet" href="../assets/css/animate.min.css" />
        <link rel="stylesheet" href="../assets/css/ionicons.min.css" />
        <link rel="stylesheet" href="../assets/css/font-awesome.min.css" />
        <!--<link rel="stylesheet" href="../assets/css/styles.css" />-->
        <link rel="stylesheet" href="../assets/css/loginstyle.css" />
        <link rel="stylesheet" href="../assets/css/sweetalert.css" />
        <link rel="stylesheet" href="../assets/css/google.css" />
    <head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3">
                    <div class="form-group central">
                        <form onsubmit="return false;" id="login">
                            <div class="row">
                                <h2>Sistema interno Key's </h2>
                                <div class="col-md-12">
                                    <label>Username:</label>
                                    <input name="username" type="text" class="form-control" />
                                </div>
                                <div class="col-md-12">
                                    <label>Password:</label>
                                    <input name="password" type="password" class="form-control" />
                                </div>
                                <div class="col-md-12" align="center">
                                    <button onclick="Access()" class="btn btn-primary loginbtn">Iniciar Sesión </button>
                                    <br>
                                    <!--<label><a>Olvidaste tu contrasena?</a></label>-->
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!--<footer>
                <div>
                    <span class="text-muted small"> ©2017 <a>Gustavo Del Castillo</a> </span>
                </div>
            </footer>-->
        <div>

        <!--scripts loaded here -->
        <script src="../assets/js/jquery.min.js"></script>
        <script src="../assets/js/validate.min.js"></script>
        <script src="../assets/js/bootstrap.min.js"></script>
        <script src="../assets/js/jquery.easing.min.js"></script>
        <script src="../assets/js/wow.js"></script>
        <script src="../assets/js/scripts.js"></script>
        <script src="../assets/js/functions.js" ></script>
        <script src="../assets/js/sweetalert.min.js" ></script>
        <script src="../assets/js/jquery.blockui.js"></script>
    </body>
</html>
