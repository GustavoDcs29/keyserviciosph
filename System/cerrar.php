<?php
    session_name('Keys');
    session_start();
    unset($_SESSION['id']);   
    unset($_SESSION['user']);  
    unset($_SESSION['nomape']);     
    session_destroy();
    header("Location: index.php");
?>