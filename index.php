<!DOCTYPE html>
<html>
  <head>
    <?php require_once 'modelo/Conexion.php';
       $pdo = new Conexion();
    ?>
    <meta charset="utf-8">
    <title> Key's Servicios y Soluciones </title>
    <meta name="description" content="Key's servicios y soluciones, administracion de propiedades horizontales" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="generator" content="Codeply">
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.png" >
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="assets/css/animate.min.css" />
    <link rel="stylesheet" href="assets/css/ionicons.min.css" />
    <link rel="stylesheet" href="assets/css/font-awesome.min.css" />
    <link rel="stylesheet" href="assets/css/styles.css" />
    <link rel="stylesheet" href="assets/css/sweetalert.css" />
    <link rel="stylesheet" href="assets/css/google.css" />
    <!--<link rel="stylesheet" href="assets/css/navstyle.css" />-->
  </head>
  <body>

    <div id="body" class="container classBody">
    <header id="first">
    <nav id="topNav" class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed " data-toggle="collapse" data-target="#bs-navbar">
                        <span class="sr-only"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a id="brand-container" class="navbar-left navbar-brand brand page-scroll bigImage" href="#first">
                        <img id="brand-logo" class="top image-responsive" src="assets/img/LOGOKEY2.png" />
                    </a>
                </div>
                <div class="navbar-collapse collapse" id="bs-navbar">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a class="page-scroll top" href="#first">Inicio</a></li>
                        <li><a class="page-scroll top" href="#four">¿Quiénes Somos?</a></li>
                        <li><a class="page-scroll top" href="#two">Servicios</a></li>
                        <li><a class="page-scroll top" href="#last">Contacto</a></li>
                        <li><a class="top" href="blog\">Blog</a></li>
                        <li><a class="top remark" href="seleccionPH.php">LOGIN</a></li>
                    </ul>
                </div>
            </div>
    </nav>


        <div id="background-carousel">
          <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
              <div class="item active" style="background-image:url('assets/img/panama_skyline.jpg'); background-size: 100% 100% ;background-repeat:no-repeat;"></div>
              <div class="item" style="background-image:url('assets/img/slide_01.jpg')"></div>
              <div class="item" style="background-image:url('assets/img/about_066.jpg'); background-size: 100% 100% ;background-repeat:no-repeat;"></div>
              <div class="item" style="background-image:url('assets/img/slide_02.jpg')"></div>
              <div class="item" style="background-image:url('assets/img/slide_03.jpg')"></div>
            </div>
          </div>
        </div>

        <div class="header-content">
            <div class="inner">
                <h1 class="cursive text-color"><b>Key's Servicios y Soluciones S.A.</b></h1>
                <h3 class="cursive" style="border-top:40px;">Servicio de Administración de Propiedad Horizontal</h3>
                <br />
                    <br />
                    <hr>
            </div>
        </div>
    </header>
    
    <div style="background-color: #646060">
        <div class="row">
            <div class="col-md-12">
            <a href="#four" class="page-scroll" style="width: 220px; text-decoration:none; color:#fff;">
                <div class="col-xs-6 text-center btn-xl d3" >
                    <h5 class="h4">Conócenos</h5>
                </div>
            </a>
            <a href="#last" class="page-scroll" style="width: 220px; text-decoration:none; color:#fff;">
                <div class="col-xs-6 text-center btn-xl d3">
                    <h5 class="h4">Contáctanos</h5>
                </div>
            </a>
            </div>
        </div>
    </div>

    <div class="well toggle" style="margin-botton:0">
        <div id="myCarouse2" class="carousel fdi-Carousel slide">
            <!-- Carousel items -->
            <div class="carousel fdi-Carousel slide" id="eventCarousel" data-interval="0">
                <div class="carousel-inner onebyone-carosel">
                    <?php
                        $query = "SELECT direccion_img FROM publicidad WHERE status = 1";
                        $rsp = $pdo->query($query);

                        $num_img = $rsp->rowCount();
                        $i = 0;
                        if($num_img > 0){
                            while($imgUrl = $rsp->fetch(PDO::FETCH_ASSOC)){
                              if($i == 0){
                                echo '<div class="item active">
                                        <div class="col-md-3">
                                            <a href="#"><img height="191px" src="'.$imgUrl["direccion_img"].'" class="img-responsive center-block"></a>
                                        </div>
                                    </div>';
                              }else{
                                echo '<div class="item">
                                        <div class="col-md-3">
                                            <a href="#"><img height="191px" src="'.$imgUrl["direccion_img"].'" class="img-responsive center-block"></a>
                                        </div>
                                    </div>';
                              }
                              $i++;
                            }
                        }else{
                            echo '<div class="item active">
                            <div class="col-md-3">
                                <a href="#"><img src="assets/img/forRent.png" class="img-responsive center-block"></a>

                            </div>
                        </div>
                        <div class="item">
                            <div class="col-md-3">
                                <a href="#"><img src="assets/img/forRent.png" class="img-responsive center-block"></a>

                            </div>
                        </div>
                        <div class="item">
                            <div class="col-md-3">
                                <a href="#"><img src="assets/img/forRent.png" class="img-responsive center-block"></a>

                            </div>
                        </div>
                        <div class="item">
                            <div class="col-md-3">
                                <a href="#"><img src="assets/img/forRent.png" class="img-responsive center-block"></a>

                            </div>
                        </div>';
                        }
                    ?>
                </div>
                <a class="left carousel-control" href="#eventCarousel" data-slide="prev"></a>
                <a class="right carousel-control" href="#eventCarousel" data-slide="next"></a>
            </div><!--/carousel-inner-->
        </div><!--/myCarouse2-->
    </div><!--/well-->
    <section class="container-fluid" id="four">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h1 class="margin-top-0 text-primary cursive"><b>¿Quiénes Somos?</b></h1>
                    <hr class="primary">
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-md-7 col-xs-12 ">
                    <div class="">
                        <br />
                        <p class="text-dark media wow fadeInLeft">
                            Key's Servicios y Soluciones, S.A, empresa de servicios de <b>ADMINISTRACION DE PROPIEDADES
                            INMOBILIARIAS</b>, con más de 10 años en el mercado panameño y referencias, que confirman la calidad
                            y confiabilidad de nuestro servicio.
                        </p>
                        <p class="text-dark media wow fadeInLeft">
                            Nuestro principal objetivo es la búsqueda de la satisfacción de las necesidades de atención y servicio
                            que exigen cada uno de nuestros clientes, por lo cual contamos con un equipo de proveedores de servicios,
                            que nos permiten atender de la manera oportuna los requerimientos que se presentan, para el mantenimiento
                            y cuidado de las áreas comunes.
                        </p>
                        <p class="text-dark media wow fadeInLeft">
                            También contamos con personal administrativo capacitado en el área de servicio al cliente,
                            comprometido con brindar un buen trato y atención, basándose en el respeto y la cordialidad.
                        </p>
                        <br />
                    </div>
                </div>
                <div class="col-lg-5 col-md-5 col-sm-12">
                    <br />
                    <h3 class="text-info text-center">Para más información &nbsp;<i class="icon-lg ion-ios-information-outline"></i></h3>
                    <div class="row" >
                        <div class="col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1" style="border: 2px solid black; border-radius: 8px">
                            <h4><i class="ion-email"></i>&nbsp;Departamento de atención al cliente:</h4>
                            <h5 style="color: orange"><b>keysserviciosysoluciones@gmail.com</b></h5>
                            <h4><i class="ion-email"></i>&nbsp;Departamento de operaciones:</h4>
                            <h5 style="color: orange"><b>Keys.operaciones@gmail.com</b></h5>
                            <h4><i class="ion-android-call"></i>&nbsp;Teléfonos:</h4>
                            <h5 style="color: orange"><b>+507 323-3237 / +507 6449-7825</b></h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="two">
      <div class="row">
          <div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3 col-md-8 col-md-offset-2">
              <h1 class="cursive text-center text-primary"><b>Servicios</b></h1>
              <hr>
              <div class="row media wow fadeInRight">
                  <h3>Gestión de Cobranza</h3>
                  <div class="col-md-8 col-sm-12  ">
                      <p>
                        La alta morosidad que usualmente presentan las cuentas por cobrar a los propietarios, se genera
                        entre otras causas, por la falta de una constante gestión de cobranza y aplicación de las leyes
                        panameñas, por ello dentro de nuestros servicios efectuamos mensualmente la <b>gestión de cobranza directa</b>
                        y con el apoyo de las Juntas Directivas y de nuestros asesores legales aplicación de las disposiciones legales
                        que dictamina la ley de Propiedad Horizontal del 2010.
                      </p>
                  </div>
                  <div class="col-md-4 col-sm-12  text-center">
                      <img height="200px" width="200px;" src="assets/img/controncontable.png" />
                  </div>
              </div>
              <hr>
              <div class="row media wow fadeInLeft">
                  <h3>Supervisión constante y mantenimiento preventivo de las áreas comunes, equipos mecánicos y eléctricos</h3>
                  <div class="col-md-4 col-sm-12  text-center">
                      <img height="200px" width="200px;" src="assets/img/mantenimiento.png" />
                  </div>
                  <div class="col-md-8 col-sm-12  ">
                      <p>
                        Los altos costos que implican las reposición o reparación de equipos, obliga que nuestra gestión administrativa
                        con el apoyo de las Juntas Directivas mantenga una constante labor de <b> mantenimiento preventivo </b>
                        y supervisión del bien funcionamiento de equipos y herramientas.
                      </p>
                  </div>
              </div>
              <hr>
              <div class="row media wow fadeInRight">
                  <h3>Procesamiento de la Información</h3>
                  <div class="col-md-8 col-sm-12  ">
                      <p>
                        Contamos con un sistema operativo, que permite gestionar de manera efectiva y eficiente toda la informacion
                        contable y operativa del PH, para la toma de decisiones (Presupuesto) y cumplimiento de las disposiciones que
                        exige la Ley de propiedad Horizontal.
                      </p>
                      <p>
                        Brindamos acceso a cada unidad inmobiliaria a su Estado de Cuenta e información que se genere
                        en el PH, a través de su telefónica móvil.
                      </p>
                  </div>
                  <div class="col-md-4 col-sm-12  text-center">
                      <img height="200px" width="200px;" class="icon-lg" src="assets/img/procesamiento.png" alt="">
                  </div>
              </div>
              <hr>
              <div class=" row media wow fadeIn">
                  <h3>Reuniones de trabajos</h3>
                  <div class="col-md-4 col-sm-12  text-center">
                      <img height="200px" width="200px;" src="assets/img/meeting.png" />
                  </div>
                  <div class="col-md-8 col-sm-12  ">
                      <p>
                        Atendiendo las necesidades de flujo de la información y las actividades que se están desarrollando,
                        ofrecemos la opción a los miembros de las Juntas Directivas de Propietarios, de poder realizar
                        reuniones de trabajos trimestrales o bimensuales, para evaluar avances de actividades
                        e intercambiar información sobre la operativa y mantenimiento del PH.
                      </p>
                  </div>
              </div>
              <hr>
              <div class="row media wow fadeInRight">
                  <h3>Asesorías: Administrativa y Legal</h3>
                  <div class="col-md-8 col-sm-12  ">
                      <P>
                        Brindamos nuestra experiencia y experticia en las labores de administración y matenimiento
                        para lograr alcanzar con el apoyo de las Juntas Directivas y propietarios el mejor rendimiento
                        de los dineros recaudados.
                      </P>
                      <p>
                        También tenemos en nuestro equipo profesionales del Derecho, que apoyan la gestión de nuestra empresa
                        y brindan asesoría a los PH.
                      </p>
                  </div>
                  <div class="col-md-4 col-sm-12  text-center">
                      <img height="200px" width="200px;" src="assets/img/asesoramiento.png" />
                  </div>
              </div>
          </div>
      </div>
    </section>

    <section id="last">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <h1 class="cursive text-primary margin-top-0"><b>Contacto</b></h1>
                    <hr class="primary">

                </div>
                <div class="col-lg-5">
                    <h3 class="text-info text-center">¡Nos pueden encontrar en!&nbsp;&nbsp;<i class="icon-lg ion-android-map"></i></h3>
                    <div id="map" class="col-xs-12 col-md-12">
                        <script type="text/javascript">
                            function initMap() {
                                var uluru = {lat: 9.008234, lng: -79.506702};
                                var map = new google.maps.Map(document.getElementById('map'), {
                                zoom: 18,
                                center: uluru
                                });
                                var marker = new google.maps.Marker({
                                position: uluru,
                                map: map
                                });
                            }
                        </script>
                        <script async defer
                            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCovvPhx4_ofKNaFdotI2ASG1QaxNa6Nx4&callback=initMap">
                        </script>
                    </div>
                    <div class="col-xs-12 col-md-12">
                      <h4><i class="ion-ios-location-outline"></i>&nbsp;&nbsp;Ciudad de Panamá</h4>
                      <h5>Av. Ernesto T Lefevre, Edificio Nº3, 2do Piso. Oficina Nº3 (Referencia Rest. Delicias Peruana)</h5>
                      <h4><i class="ion-ios-clock-outline"></i>&nbsp;&nbsp;Horario de Atención</h4>
                      <h5>De Lunes a Viernes: 8:30am a 12m – 2:00pm a 4:00pm</h5>
                      <h4><i class="ion-android-call"></i>&nbsp;Teléfonos:</h4>
                      <h5>+507 323-3237 / +507 6449-7825</h5>
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="col-lg-10 col-lg-offset-1 text-center">
                        <p style="margin-top:10px;">
                        Nos encantaría poder atenderles, por favor complete el formulario a continuación y le estaremos
                        respondiendo lo más pronto posible.
                        <h3 class="text-info">¡Gracias!</h3>
                        </p>
                        <form id="Contacto" onsubmit="" class="contact-form row">
                            <div class="col-md-4">
                                <label></label>
                                <input id="name" name="name" type="text" class="form-control" placeholder="Nombre Completo">
                            </div>
                            <div class="col-md-4">
                                <label></label>
                                <input id="email" name="email" type="text" class="form-control" placeholder="Correo Electronico">
                            </div>
                            <div class="col-md-4">
                                <label></label>
                                <input id="telephone" name="telephone" type="text" class="form-control" placeholder="Telefono">
                            </div>
                            <div class="col-md-12">
                                <label></label>
                                <textarea id="message" name="message" class="form-control" rows="9" placeholder="Escriba su mensaje aqui. . ."></textarea>
                            </div>
                            <div class="col-md-4 col-md-offset-4">
                                <label></label>
                                <button type="submit" class="d3 btn btn-block btn-lg" style="color:#fff;">Enviar &nbsp; <i class="ion-paper-airplane"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div id="contenedor"></div>
        </div>
    </section>
    <footer id="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-6 col-sm-4 column">
                    <h4>Páginas</h4>
                    <ul class="list-unstyled">
                        <li><a class="page-scroll" href="#first">Inicio</a></li>
                        <li><a class="page-scroll" href="#four">¿Quiénes somos?</a></li>
                        <li><a class="page-scroll" href="#two">Servicios</a></li>
                        <li><a class="page-scroll" href="#last">Contacto</a></li>
                        <li><a class="page-scroll" >Noticias</li>
                        <li><a class="page-scroll" href="seleccionPH.php">Login</a></li>
                    </ul>
                </div>
                <div class="col-xs-6 col-sm-4 column text-center">
                    <h4>Noticias Recientes</h4>
                    <!--<ul class="list-unstyled">
                        <li><a href="#">Noticia 1</a></li>
                        <li><a href="#">Noticia 2</a></li>
                        <li><a href="#">Noticia 3</a></li>
                        <li><a href="#">Noticia 4</a></li>
                    </ul>-->
                </div>
                <div class="col-xs-12 col-sm-4 text-right">
                    <h4>¡Siguenos en nuestras Redes!</h4>
                    <ul class="list-inline social-net">
                      <li><a rel="nofollow" target="_blank" href="https://twitter.com/keyspanama" title="Twitter">
                      <img style="border-radius:3px;" src="assets/img/Icons/twitter.png" />
                      <!--<i class="fa fa-twitter-square fa-4x"></i>--></a>&nbsp;</li>
                      <li><a rel="nofollow" target="_blank" href="https://facebook.com/key.spanama" title="Facebook">
                      <img src="assets/img/Icons/facebook.png" />
                      <!--<i class="fa fa-facebook-official fa-4x"></i>--></a>&nbsp;</li>
                      <li><a rel="nofollow" target="_blank" href="https://www.instagram.com/keyspanama" title="Instagram">
                      <img src="assets/img/Icons/instagram2.png" />
                      <!--<i class="fa fa-instagram fa-4x"></i>--></a></li>
                    </ul>
                </div>
            </div>
            <br/>
            <span class="pull-right text-muted small"> ©2017 <a>Key's Servicios Y Soluciones S.A</a> </span>
        </div>
    </footer>

        <!--scripts loaded here -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/validate.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/jquery.easing.min.js"></script>
        <script src="assets/js/wow.js"></script>
        <script src="assets/js/scripts.js"></script>
        <script src="assets/js/functions.js" ></script>
        <script src="assets/js/sweetalert.min.js" ></script>
        <script type="text/javascript">
            $(window).scroll(function(){
                var PositionTop = $(document).scrollTop();
                if(PositionTop > 10){
                    $('#brand-container').removeClass('bigImage');
                }else{
                    $('#brand-container').addClass('bigImage');
                }
            });
        </script>
    </div>
  </body>
</html>
