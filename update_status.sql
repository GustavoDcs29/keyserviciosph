USE `keyserviciosassets`;
DROP procedure IF EXISTS `update_status`;

DELIMITER $$
USE `keyserviciosassets`$$
CREATE PROCEDURE `update_status` ()
BEGIN
	DECLARE today_date DATE DEFAULT current_date();
    DECLARE end_date DATE;
    DECLARE num_row INT;
    DECLARE i INT DEFAULT 1;

    SELECT COUNT(*) FROM publicidad INTO num_row;

    ACTUALIZACION : LOOP
		SELECT date_f FROM publicidad WHERE id_publicidad = i INTO end_date;

        IF today_date = end_date
			THEN
				UPDATE publicidad SET status = 0 WHERE id_publicidad = i;
		END IF;

        SET i = i + 1;
        IF i = num_row + 1 THEN LEAVE ACTUALIZACION;

        END IF;
    END LOOP;
END$$

DELIMITER ;

SHOW GLOBAL VARIABLES;

SET GLOBAL event_scheduler = ON;

CREATE EVENT CHECKIN_DAILY2
	ON SCHEDULE
		EVERY 1 MINUTE
	DO
		CALL `keyserviciosassets`.`update_status`();


DROP EVENT IF EXISTS CHECKIN_DAILY;
