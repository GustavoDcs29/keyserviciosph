<!DOCTYPE html>
<html lang="en" ng-app="app-blog">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Key's Blog</title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Theme CSS -->
    <link href="css/clean-blog.min.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/sweetalert.css" />
    <!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
    <script data-require="angular-ui-bootstrap@0.3.0" data-semver="0.3.0" src="http://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.3.0.min.js"></script>
    <script type="text/javascript" src="app.js" ></script>
</head>

<body >

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-custom navbar-fixed-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                     <i class="fa fa-bars"></i>
                </button>
                <!--<img id="brand-logo" class="top image-responsive" src="../assets/img/LOGOKEY2.png" />-->
                <a class="navbar-brand" href="../index.php">Key's Servicios PH</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <input style="margin-top:15px;" class="form-control" type="text" id="buscar_post" placeholder="Buscar una publicación">       
                    </li>
                    <li>
                        <a class="navbar-brand" href="">Buscar</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Header -->
    <!-- Set your background image for this header on the line below. -->
    <header class="intro-header" style="background-image: url('img/home-bg.jpg')">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <div class="site-heading">
                        <h1>Key's Blog</h1>
                        <hr class="small">
                        <span class="subheading">El mejor lugar para las noticias sobre su ph</span>
                    </div>
                </div>
            </div>
        </div>
    </header>


    <!-- Main Content -->
    <div class="container" >
        <div class="row">
            <div ng-controller="PageCtrlr" class="col-lg-8  col-md-10 " >
                <div ng-repeat="publicacion in filteredPosts" class="post-preview">
                    <a href="post.php?id_publicacion={{ publicacion.idnoticia }}">
                        <h2 class="post-title">
                            {{ publicacion.titulo }}
                        </h2>
                        <h3 class="post-subtitle">
                            {{ publicacion.subtitulo }}
                        </h3>
                        </a>
                         <p class="post-meta">
                             Publicado por <a href="#"> {{ publicacion.autor }} </a> - {{ publicacion.fec_emis }} 
                        </p>
                    </a>
                    <hr>
                </div>
                
                <div align="center" class="pager" data-pagination="" data-num-pages="numPages()" data-current-page="currentPage" data-max-size="maxSize" data-boundary-links="false">
                </div>
            </div>
            <div ng-controller="SubcriptionsCtrl" class="col-lg-4 col-md-2" style="background-color:#0085A1; padding:10px;">
                <h4 style="color:white;"> No te pierdas de todas nuestras novedades! </h4>
                <form id="subsForm" ng-submit="saveSubscriber()">
                    <div class="row">
                        <div class="col-md-12">
                            <input type="email" ng-model="formData.correo" class="form-control" placeholder="Ingrese aquí su correo electronico" required/>
                        </div>
                    </div>
                    <br/>
                    <div class="row" align="right">
                        <div class="col-md-12">
                            <button class="btn btn-warning">Subscribete!</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <hr>

    <!-- Footer -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <ul class="list-inline text-center">
                        <li>
                            <a href="#">
                                <span class="fa-stack fa-lg">
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span class="fa-stack fa-lg">
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span class="fa-stack fa-lg">
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <i class="fa fa-instagram fa-stack-1x fa-inverse"></i>
                                </span>
                            </a>
                        </li>
                    </ul>
                    <p class="copyright text-muted">Copyright &copy;2018 Key's Servicios y Soluciones S.A.</p>
                </div>
            </div>
        </div>
    </footer>

    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <!-- Theme JavaScript -->
    <script src="js/clean-blog.min.js"></script>
    <script src="js/ngSweetAlert/SweetAlert.min.js" ></script>
    <style>
    .bigImage{
        height: 80px;
    }
    </style>
    <script type="text/javascript">
            $(window).scroll(function(){
                var PositionTop = $(document).scrollTop();
                if(PositionTop > 10){
                    $('#brand-container').removeClass('bigImage');
                }else{
                    $('#brand-container').addClass('bigImage');
                }
            });
    </script>
</body>

</html>
