<?php
    require 'modelo/Conexion.php';
    require 'modelo/Functions.php';

    $pdo = new Conexion();

    session_name('temp');
    session_start();
    
    $fileName = $_FILES['file']['name'][0];
    $fileTempName = $_FILES['file']['tmp_name'][0];

    $target_dir = "uploads/";
    $target_file = $target_dir . basename($fileName);
    $uploadOk = 1;
    $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);

    if(isset($_POST['submit'])){
        $check = getimagesize($fileTempName);

        if($check !== false){
            //echo "File is an image - " . $check['mime'] . " . ";
            $uploadOk = 1 ;
        }
        else{
            //echo "File is not an image";
            $uploadOk = 0;
        }
    }
    //If there is an error the upload could be canceled
    if($uploadOk == 0){
        echo "Sorry your file was not uploaded";
    }
    //if everything is ok the upload should work normally
    else{
        if(move_uploaded_file($fileTempName , $target_file)){
             /**
             * Code to insert the information about the advertisement on database 
             */
            if(updateTemp($_SESSION['LastId'] , $target_file)){
                if(setAdvertisement($_SESSION['LastId'])){
                    if(deleteTemp($_SESSION['LastId'])){
                        echo "The file " . basename($fileName) . " has been uploaded.";
                    }
                }
            }
        }
        else{
            echo "Sorry there was an error uploading your file";
        }
    }
?>